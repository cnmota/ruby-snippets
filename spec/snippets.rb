require "./lib/snippets"

describe Snippets do 
  describe "anonymous_block_as_argument" do
    context "call using variable" do

      it do 
        expect(Snippets::anonymous_block_as_argument("nuno")).to eq("nuno")
      end
    end
    context "call using anonymous block" do

      it do 
        expect(Snippets::anonymous_block_as_argument { "mota" }).to eq("mota")
      end
    end
  end
end

describe Snippets::StaticAndObject do
  describe "#i_am_object_method / # i_am_static_method" do
    context "on instance" do
      let(:obj) { Snippets::StaticAndObject.new }

      it do 
        expect(obj.respond_to?(:i_am_object_method)).to eq(true)
        expect(obj.respond_to?(:i_am_static_method)).to eq(false)
      end
    end
    context "on class" do
      let(:cls) { Snippets::StaticAndObject }

      it do 
        expect(cls.respond_to?(:i_am_object_method)).to eq(false)
        expect(cls.respond_to?(:i_am_static_method)).to eq(true)
      end
    end
  end
end

describe Snippets::Person do
  describe "to_json()" do
    context "empty object" do
      let(:person) { Snippets::Person.new }

      it do 
        expect(person.to_json).to eq("{}")
      end
    end
    context "object with only first name" do
      let(:person) { Snippets::Person.new(:name => "Nuno") }

      it do 
        expect(JSON.parse(person.to_json)).to eq(JSON.parse("{\"first_name\":\"Nuno\"}"))
      end
    end
    context "object with both first and last name" do
      let(:person) { Snippets::Person.new(:name => "Nuno Mota") }

      it do 
        expect(JSON.parse(person.to_json)).to eq(JSON.parse("{\"first_name\":\"Nuno\",\"last_name\":\"Mota\"}"))
      end
    end
  end
  describe "from_json()" do
    context "empty JSON" do
      let(:person) { Snippets::Person::new_from_json('{}') }

      it do
        expect(person.first_name.nil?).to eq(true)
      end
    end
    context "With both first and last name" do
      let(:person) { Snippets::Person::new_from_json('{"first_name" : "First", "last_name": "Last" }') }

      it do
        expect(person.first_name.nil?).to eq(false)
        expect(person.last_name.nil?).to eq(false)
        expect(person.first_name).to eq("First")
        expect(person.last_name).to eq("Last")
      end
    end
  end
end