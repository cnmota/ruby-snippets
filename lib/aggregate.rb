require "./lib/event_register.rb"

class Aggregate
  include EventRegister    
  attr  :id, :type, :version

  def causes(event)
    self.register_event(event)
  end
end