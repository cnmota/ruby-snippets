module EventRegister
  attr  :events

  def register_event(event)
    @events = [] if @events.nil?
    @events.push(event)
  end

  def reset_events()
    @events.clear
  end
end