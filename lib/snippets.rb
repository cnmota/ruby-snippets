require 'rspec'
require 'json'

module Snippets
  def self.anonymous_block_as_argument( sentence = nil ) 
    if block_given?
      yield
    else 
      return sentence
    end
  end

  class StaticAndObject
    def self.i_am_static_method
        return true
    end
    def i_am_object_method
        return true
    end
  end

  class Person
    attr_accessor :first_name, :last_name, :age

    def initialize(params = {})
      @first_name, @last_name = params[:name].split(/ /) unless params[:name].nil?

      @age = nil
    end

    def to_json() 
      hash = {}

      hash[:first_name] = @first_name unless @first_name.nil?
      hash[:last_name] = @last_name unless @last_name.nil?
      hash[:age] = @age unless @age.nil?

      return JSON.generate(hash)
    end 

    def self.new_from_json(bytes) 
      hash = JSON.load(bytes)
      
      p = Person.new
      p.first_name = hash["first_name"]
      p.last_name = hash["last_name"]

      return p
    end 
  end
end
